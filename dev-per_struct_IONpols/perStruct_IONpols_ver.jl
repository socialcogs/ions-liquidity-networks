function add_ION_capacity(ion, capacity, price)
    # Add to all my offers
    for (myOffNm, myOff) in myOffers
        println(string(now()) * " @" * myID *
            ": For my own offer {" * myOffNm * "}:")
        if !haskey(myOff.suppl_IONpol, ion)
            myOff.suppl_IONpol[ion] = Dict{String,Float64}()
            println(string(now()) * " @" * myID *
                ": ==> Starting to accept {" * ion * "} for offer {" * myOffNm * "}")
        end
        if haskey(myOff.suppl_IONpol[ion], "max_accept")
            myOff.suppl_IONpol[ion]["max_accept"] += capacity
            println(string(now()) * " @" * myID *
                ": ==> Capacity for {" * ion * "} increased by {" * string(capacity) * "}\n")
        else
            myOff.suppl_IONpol[ion]["max_accept"] = capacity
            println(string(now()) * " @" * myID *
                ": ==> Capacity for {" * ion * "} set to {" * string(capacity) * "}\n")
            myOff.suppl_IONpol[ion]["ION_price"] = price
        end
    end

    # Add to all my accReceiv
    for (payerID, accRecords) in accReceiv
        for accRec in accRecords
            println(string(now()) * " @" * myID *
                ": For my invoice {" * accRec.invID * "}:")
            if !haskey(accRec.payReceiv_IONpol, ion)
                accRec.payReceiv_IONpol[ion] = Dict{String,Float64}()
                println(string(now()) * " @" * myID *
                    ": ==> Starting to accept {" * ion * "} for invoice {" * accRec.invID * "}")
            end
            if haskey(accRec.payReceiv_IONpol[ion], "max_accept")
                accRec.payReceiv_IONpol[ion]["max_accept"] += capacity
                println(string(now()) * " @" * myID *
                    ": ==> Capacity for {" * ion * "} increased by {" * string(capacity) * "}\n")
            else
                accRec.payReceiv_IONpol[ion]["max_accept"] = capacity
                println(string(now()) * " @" * myID *
                    ": ==> Capacity for {" * ion * "} set to {" * string(capacity) * "}\n")
                accRec.payReceiv_IONpol[ion]["ION_price"] = price
            end
        end
    end
end

function remove_ION_capacity(ion, capacity)
    # Remove from all my offers
    for (myOffNm, myOff) in myOffers
        if haskey(myOff.suppl_IONpol, ion)
            if haskey(myOff.suppl_IONpol[ion], "max_accept")
                println(string(now()) * " @" * myID *
                    ": For offer {" * myOffNm * "}:")
                myOff.suppl_IONpol[ion]["max_accept"] -= capacity
                if myOff.suppl_IONpol[ion]["max_accept"] < 0
                    myOff.suppl_IONpol[ion]["max_accept"] = 0
                end
                println(string(now()) * " @" * myID *
                    ": ==> Capacity for {" * ion * "} reduced by {" * string(capacity) * "}\n")
            end
        end
    end

    # Remove from all my accReceiv
    for (payerID, accRecords) in accReceiv
        for accRec in accRecords
            if haskey(accRec.payReceiv_IONpol[ion], "max_accept")
                println(string(now()) * " @" * myID *
                    ": For invoice {" * accRec.invID * "}:")
                accRec.payReceiv_IONpol[ion]["max_accept"] -= capacity
                if accRec.payReceiv_IONpol[ion]["max_accept"] < 0
                    accRec.payReceiv_IONpol[ion]["max_accept"] = 0
                end
                println(string(now()) * " @" * myID *
                    ": ==> Capacity for {" * ion * "} reduced by {" * string(capacity) * "}\n")
            end
        end
    end
end
matchCost = demOb.unitsWanted * updOffOb.unitCost
### fragment

for (kUpd, vUpd) in updOffOb.updateDesc
    println(string(now()) * " @" * myID *
        ": The offer has a new update, saying: {" * kUpd * "}, " * vUpd * "\n")

    # currently updateDesc handles only ION key updates
    # so kUpd is an ION id
    if vUpd == "ION now accepted"
        peer_accept_max = updOffOb.suppl_IONpol[kUpd]["max_accept"]
        ION_price = updOffOb.suppl_IONpol[kUpd]["ION_price"]
        match_ION_cost = matchCost / ION_price
        # link with this peer can carry as much as
        link_ION_capacity = min(match_ION_cost, peer_accept_max)
        add_ION_capacity(kUpd, link_ION_capacity, ION_price)
        my_policies_on_IONs_have_changed = true
    elseif vUpd == "ION max_accept amount change"
        # get the old/current offer max_accept
        # from my internal marketOffers store:
        oldCapacity = marketOffers[updOffID].suppl_IONpol[kUpd]["max_accept"]
        remove_ION_capacity(kUpd, oldCapacity)
        # the updated capacity
        peer_accept_max = updOffOb.suppl_IONpol[kUpd]["max_accept"]
        ION_price = updOffOb.suppl_IONpol[kUpd]["ION_price"]
        match_ION_cost = matchCost / ION_price
        # link with this peer can carry as much as
        link_ION_capacity = min(match_ION_cost, peer_accept_max)
        add_ION_capacity(kUpd, link_ION_capacity, ION_price)
        my_policies_on_IONs_have_changed = true
    elseif vUpd == "ION not accepted anymore"
        # get the old/current offer max_accept
        # from my internal marketOffers store:
        oldCapacity = marketOffers[updOffID].suppl_IONpol[kUpd]["max_accept"]
        remove_ION_capacity(kUpd, oldCapacity)
        my_policies_on_IONs_have_changed = true
    end
end
