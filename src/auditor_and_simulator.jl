# I don't need the graph model for the MVP - only when I start adding intelligence and advanced simulator functions

# pythonX -m pip install xmltodict
xml2dict = pyimport("xmltodict")
y = xml2dict.parse(x)
z = y["mxGraphModel"]["root"]["mxCell"]
# can setup a dictionary of node/edgeIDs to z index
# e.g. z[4]["@value"]="Awesome"

backToXML_singleLine = xml2dict.unparse(y)
backToXML_pretty = xml2dict.unparse(y, pretty=true)
# E.g. mydict = {
# ...     'XMLtag': {
# ...         '@color':'red',
# ...         '@stroke':'2',
# ...         '#text':'This is a test'
# ...     }
# ... }
# (? or if not working, try #XMLtag instead of #text)
# >>>>>>>>>
# <XMLtag stroke="2" color="red">This is a test</XMLtag>