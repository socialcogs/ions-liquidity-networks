using HTTP
using JSON2
using Dates
using Random
using UUIDs
using JLD2
 
rng = MersenneTwister(1234)

const myID = ARGS[1]
const pullFreq = parse(Int, ARGS[2])

mutable struct IONpol_node_time
    timestamp::DateTime
    nodeID::String
    #IONpol of a peer
    peer_IONpol::Dict{String, Dict{String, Float64}}
    # Regardless of how many IONs this peer accepts,
    # how many would I spend max with this peer
    myMax::Float64
end

mutable struct Offer
    timestamp::DateTime
    offerName::String
    offerBy::String
    desc::String
    # Unit cost in $
    unitCost::Float64
    # ex: suppl_IONpol["CHOC"] = ("max_accept"=>"5")
    suppl_IONpol::Dict{String, Dict{String, Float64}}
end

mutable struct Demand
    timestamp::DateTime
    demandName::String
    demandBy::String
    desc::String
    unitsWanted::Float64
    # if offered price is below this, offer auto-matched:
    autoMatchMax::Float64
    # ex: deman_IONpol["CHOC"] = ("max_accept"=>"5")
    deman_IONpol::Dict{String, Dict{String, Float64}}
end

# My own demands
#   Dict key is only demand name (without nodeID)
const myDemands = Dict{String, Demand}()
# Mu own offers
#   Dict key is only offer name (without nodeID)
const myOffers = Dict{String, Offer}()

# My local store of offers of others
#   Dict key is full public ID, with <<nodeID
const marketOffers = Dict{String, Offer}()

# demandName-offerIDs CANDIDATES map
#    offerID = offerName<<offerBy
# Dict{OfferID, String} to be used to store versioning & other intelligence
const dem_offs_match = Dict{String, Dict{String, String}}()

const IONs_I_mint = Set()

# Which ION I can use in payments and the policies (max_accept, ION_price)
# note this includes both IONs I terminate and IONs I pass-through
global myIONpol = Dict{String, Dict{String, Float64}}()

function Add_Demand(name, unitsWanted, autoMatchMax)
    myDemands[name] = Demand(now(), name, myID, "some description...", unitsWanted, autoMatchMax, Dict{String, Dict{String, Float64}}())
    pullRelay_fullMarket()
end

function Add_Offer(name, unitCost, ion, ION_price, max_accept)
    push!(IONs_I_mint, ion)
    myOffers[name] = Offer(now(), name, myID, "some description...", unitCost, Dict(ion=>Dict("ION_price"=>ION_price, "max_accept"=>max_accept)))
    HTTP.request("PUT", "http://127.0.0.1:8081/updateOffers/"*myID, [], JSON2.write(myOffers))
end

function Add_Offer(name, unitCost)
    myOffers[name] = Offer(now(), name, myID, "some description...", unitCost, Dict{String, Dict{String, Float64}}())
    HTTP.request("PUT", "http://127.0.0.1:8081/updateOffers/"*myID, [], JSON2.write(myOffers))
end

# after offer-demand executed, AccRecord should be singular/shared
mutable struct AccRecord
    timestamp::DateTime
    invID::String
    paySender::String
    payReceiv::String
    balance::Float64
    # ex: payReceiv_IONpol["CHOC"] = ("max_accept"=>5)
    payReceiv_IONpol::Dict{String, Dict{String, Float64}}
end

# keyed by payReceiv nodeID
const accPayable = Dict{String, Vector{AccRecord}}()
# keyed by paySender nodeID
const accReceiv = Dict{String, Vector{AccRecord}}()

function Send_Invoice(paySender, balance)
    invoiceUUID = string(uuid1(rng))
    accRec = AccRecord(now(), invoiceUUID, paySender, myID, balance, Dict{String, Dict{String, Float64}}())
    accReceiv[paySender] = Vector{AccRecord}()
    push!(accReceiv[paySender], accRec)
    # Sends one accounting record to one paySender
    HTTP.request("POST", "http://127.0.0.1:8081/message_paySender/" * paySender, [], JSON2.write(accRec))
end


mutable struct ION_contract
    contract_uuid::String
    ion_name::String
    ion_price::Float64
    offerID::String
    creation_timestamp::DateTime
    # Current implementation allows for a single minter
    allowed_minters::String
    # Nodes that can spend this ion & who participate in its management. Dictionary holds how much each node has of the contract.
    stakeholders::Dict{String, Float64}
    # The total units across all stakeholders
    total::Float64
    # Additional fields would vary depending on how the stakeholders of any contract chose to manage it. Here in the simulator we use the simplest form of minter discretion with stakeholder revocation if requested.
end

# ex: wallet["CHOC"][uuid] = ION_contract
const wallet = Dict{String, Dict{String, ION_contract}}()

function Mint_IONs(offer_name, ion_name, num_units)
    num_units = convert(Float64, num_units)
    # Single contract version
    if !haskey(wallet, ion_name)
        wallet[ion_name] = Dict{String, ION_contract}()
    end
    if length(wallet[ion_name]) == 0
        # Creating a new contract if none exists
        uuid = string(uuid1(rng))
        ion_price = myOffers[offer_name].suppl_IONpol[ion_name]["ION_price"]
        new_contract = ION_contract(uuid, ion_name, ion_price, offer_name*"<<"*myID, now(), myID, Dict("C"=>num_units), num_units)
        wallet[ion_name][uuid] = new_contract
        # Register new contract addition with the Relay.
        HTTP.request("PUT", "http://127.0.0.1:8081/newIONcontract", [], JSON2.write(new_contract))
        println(string(now()) * " @" * myID *
            ": No existing contract found. Started contract {" * uuid * "} and minted the requested IONs into it.\n")
    elseif length(wallet[ion_name]) == 1
        # Increasing the latest contract
        # Who is allowed to do this and when is fully custom to each stakeholder group (contract). For the simulator we use a simple policy of at minter discretion with stakeholder revocation if requested.

        # single contract expected
        for key in keys(wallet[ion_name])
            uuid = key
        end
        wallet[ion_name][uuid].stakeholders[myID] += num_units
        wallet[ion_name][uuid].total += num_units

        # Register contract change with the Relay. Separate call from adding a contract because it might involve very different stakeholder communication.
        HTTP.request("PUT", "http://127.0.0.1:8081/updateIONcontract/minter_adding_ION_units", [], JSON2.write(wallet[ion_name][uuid]))
        println(string(now()) * " @" * myID *
            ": Minted the requested IONs into existing contract {" * uuid * "}\n")
    else
        # Chosing amongst multiple contracts with different stakeholder policies
        println("ERROR: Feature not available yet")
    end
end

# mutable struct Supply_or_Demand_Components
#     # Dictionaries allow for attributes insertion later
#     # ex: s_d_names[{x_name_WITHOUT<<nodeID}] = string_of_attributes
#     s_d_names::Dict{String, String}
#     # keyes by invoice uuid
#     invoiceIDs::Dict{String, String}
# end

mutable struct Flow_Setup_Message
    flow_uuid::String
    ion_name::String
    ion_price::Float64
    flow_state::String
    # In $s
    flow_size::Float64
    ion_contract_uuid::String
    sent_from::String
    # Relay processes and forwards to this node
    next_node::String
    # Flows run from demand components to supply components (can be thought of as the direction of payments-made)
    # The demand components of this flow leg (demands, account_payables)
    inflow_comp::Dict{String, String}
    # The supply components of this flow leg (offers, account_receivables)
    outflow_comp::Dict{String, String}
end

# Flow setup messages that are pending
# these are kept for accounting actions determination once Relay confirms them
# - keyed by flow_uuid
const pending_FSM = Dict{String, Flow_Setup_Message}()

# ex: ION_flow_candidates["CHOC"][nodeID] = capacity in ION units
const ION_flow_candidates = Dict{String, Dict{String, Float64}}()

# How much I can terminate in $ for any ION
const ION_terminate_cand = Dict{String, Float64}()

function Order_and_Pay_using_IONs(offerID, ion_name, ion_quant)
    # Send flow_start_proposal to Relay
    flow_uuid = string(uuid1(rng))

    # This funcion call assumes there is only one contract
    ion_contract_uuid = ""
    for key in keys(wallet[ion_name])
        ion_contract_uuid = key
    end

    ion_price = marketOffers[offerID].suppl_IONpol[ion_name]["ION_price"]
    flow_size = ion_quant * ion_price
    # Assumed you have a demand under the same name, since no UI in simulator
    (demNm, offBy) = split(offerID, "<<")
    inflow_comp = Dict(demNm=>"market_entry")
    outflow_comp = Dict(demNm=>"market_entry")
    fsMsg = Flow_Setup_Message(flow_uuid, ion_name, ion_price, "flow_start_proposal", flow_size, ion_contract_uuid, myID, marketOffers[offerID].offerBy, inflow_comp, outflow_comp)
    HTTP.request("PUT", "http://127.0.0.1:8081/sendFlowSetupMsg", [], JSON2.write(fsMsg))
    println(string(now()) * " @" * myID *
        ": Started a flow proposal {" * flow_uuid * "}\n")
end


function pullRelay_myMarketUpdate()
    httpGET = HTTP.request("GET", "http://127.0.0.1:8081/getMyMarketUpdate/"*myID, [])
    httpGET_string = String(httpGET.body)
    offersUpdates = JSON2.read(httpGET_string, Dict{String, Offer})
    processRelay(offersUpdates)
end

function pullRelay_fullMarket()
    httpGET = HTTP.request("GET", "http://127.0.0.1:8081/getFullMarket", [])
    httpGET_string = String(httpGET.body)
    all_market_offers = JSON2.read(httpGET_string, Dict{String, Offer})
    processRelay(all_market_offers)
end

function processRelay(offersSet)
    #println(string(now()) * " @" * myID * ": pulling\n")

    peer_IONpols_have_changed = false

    ### Accounting messages pull
    # note could later include messages from paySenders and payReceivs
    httpGET = HTTP.request("GET", "http://127.0.0.1:8081/getMyAccountingMessages/"*myID, [])
    httpGET_string = String(httpGET.body)
    accMessages = JSON2.read(httpGET_string, Vector{AccRecord})
    if length(accMessages) > 0
        println(string(now()) * " @" * myID *
            ": Got accounting messages from Relay\n")
        println("Accounting messages in update: ", accMessages)
        println("\n")
        for accMess in accMessages
            # Messages from payReceivers
            if accMess.paySender == myID
                # Expecting only ion policy updates for now
                # update IONpol for this payReceiv
                is_it_new_invoice = true
                if !haskey(accPayable, accMess.payReceiv)
                    accPayable[accMess.payReceiv]= Vector{AccRecord}()
                end
                for accRec in accPayable[accMess.payReceiv]
                    if accRec.timestamp < accMess.timestamp
                        # Assuming only these 2 fields can change
                        # - if others were to change, we can't just copy them but must engage the user in the app UIs
                        accRec.payReceiv_IONpol = accMess.payReceiv_IONpol
                        accRec.timestamp = accMess.timestamp
                        peer_IONpols_have_changed = true
                        println(string(now()) * " @" * myID *
                            ": ION policy for {" * accMess.payReceiv * "} has changed - will process it.\n")
                    end
                    if accRec.invID == accMess.invID
                        is_it_new_invoice = false
                    end
                end
                if is_it_new_invoice
                    # auto-accepts in simulator. with user apps, users will be engaged for confirmation
                    push!(accPayable[accMess.payReceiv], accMess)
                end
            end
            # Later, messages from paySenders, ex. for orders and other events
        end
    end

    # Flow setup messages pull
    httpGET = HTTP.request("GET", "http://127.0.0.1:8081/getFlowSetupMsg/"*myID, [])
    httpGET_string = String(httpGET.body)
    flowMessages = JSON2.read(httpGET_string, Vector{Flow_Setup_Message})
    if length(flowMessages) > 0
        println(string(now()) * " @" * myID *
            ": Got Flow Setup messages from Relay\n")
        println("Flow Setup messages in update: ", flowMessages)
        println("\n")
        for fsMsg in flowMessages
            # if final confirmation from the Relay
            if fsMsg.flow_state == "flow_confirmation"
                println(string(now()) * " @" * myID *
                    ": Flow {" * fsMsg.flow_uuid * "} confirmed by the Relay\n")

                # actions based on stored fsMsg

            # else it is a flow_proposal type
            else
                pending_FSM[fsMsg.flow_uuid] = deepcopy(fsMsg)
                remain_flowSz = fsMsg.flow_size
                # if I want this ION > send flow_end_proposal
                if in(fsMsg.ion_name, keys(ION_terminate_cand)) && ION_terminate_cand[fsMsg.ion_name] >= remain_flowSz
                    println(string(now()) * " @" * myID *
                        ": I am a possible terminal for {" * fsMsg.ion_name * "} IONs\n")
                    fsMsg.flow_state = "flow_end_proposal"
                    fsMsg.sent_from = myID
                    fsMsg.next_node = ""
                    fsMsg.inflow_comp = fsMsg.outflow_comp
                    fsMsg.outflow_comp = Dict{String, String}()
                    HTTP.request("PUT", "http://127.0.0.1:8081/sendFlowSetupMsg", [], JSON2.write(fsMsg))
                    println(string(now()) * " @" * myID *
                        ": ==> Sent {flow_END_proposal} to Relay\n")
                    # Current way of preventing below's execution while leaving the form for later tree flows
                    # - above used up all the available flow size
                    remain_flowSz = 0
                end
                # if I don't want it myself, assume I'm getting this because I've reported myself as a possible passthrough
                if remain_flowSz > 0
                    println(string(now()) * " @" * myID *
                        ": I am a pass-through for {" * fsMsg.ion_name * "} IONs\n")
                    fsMsg.flow_state = "flow_leg_proposal"
                    fsMsg.sent_from = myID
                    for (peer_cand, capacity_in_IONs) in ION_flow_candidates[fsMsg.ion_name]
                        capacity = capacity_in_IONs * fsMsg.ion_price
                        if capacity >= fsMsg.flow_size
                            fsMsg.next_node = peer_cand
                        end
                    end
                    fsMsg.inflow_comp = fsMsg.outflow_comp
                    # Prioritize accounting settlments
                    if haskey(accPayable, fsMsg.next_node)
                        for accR in accPayable[fsMsg.next_node]
                            if accR.balance >= fsMsg.flow_size
                                fsMsg.outflow_comp = Dict(accR.invID=>"accounting_entry")
                                remain_flowSz = 0
                            end
                        end
                    end
                    if remain_flowSz > 0
                        for (demNm, offerDict) in dem_offs_match
                            for (offID, offAttr) in offerDict
                                (offNm, offBy) = split(offID, "<<")
                                if offBy == fsMsg.next_node
                                    offerCapacity = marketOffers[offID].unitCost * myDemands[demNm].unitsWanted
                                    if offerCapacity >= fsMsg.flow_size
                                        fsMsg.outflow_comp = Dict(offNm=>"market_entry")
                                    end
                                end
                            end
                        end
                    end
                    HTTP.request("PUT", "http://127.0.0.1:8081/sendFlowSetupMsg", [], JSON2.write(fsMsg))
                    println(string(now()) * " @" * myID *
                        ": ==> Sent {flow_LEG_proposal} to Relay with next node {" * fsMsg.next_node  * "} and its outflow_comp component {" * string(fsMsg.outflow_comp) * "}\n")
                end
            end
        end
    end


    # Process Offers
    if !isempty(offersSet)
        println(string(now()) * " @" * myID *
            ": Got offers updates from Relay\n")
        println("Offers in update: ", offersSet)
        println("\n")
    end

    for (updOffID,updOffOb) in offersSet
        (updOffNm, updOffBy) = split(updOffID, "<<")
        for (demNm,demOb) in myDemands
            # if a demand-offer match:
            # and also ignoring own offers sent to me by error
            if (demNm == updOffNm) && (updOffBy != myID)
                println(string(now()) * " @" * myID *
                    ": Demand {" * demNm * "} matches offer {" * updOffID * "}\n")

                # if this is an update to an existing match
                if haskey(dem_offs_match, demNm) && haskey(dem_offs_match[demNm], updOffID)
                    println(string(now()) * " @" * myID *
                        ": But we've matched it before. We'll check if it has newer information. \n")
                    if marketOffers[updOffID].timestamp < updOffOb.timestamp
                        # Assuming only these 2 fields can change
                        # - if others were to change, we can't just copy them but must engage the user in the app UIs
                        marketOffers[updOffID].suppl_IONpol = updOffOb.suppl_IONpol
                        marketOffers[updOffID].timestamp = updOffOb.timestamp

                        peer_IONpols_have_changed = true
                        println(string(now()) * " @" * myID *
                            ": ION policy for {" * updOffBy * "} has changed - will process it.\n")
                    end
                # otherwise this is a potential new match:
                else
                    new_match_confirmed = false
                    if updOffOb.unitCost <= demOb.autoMatchMax
                        println(string(now()) * " @" * myID *
                            ": Auto-confirming because unitCost of {\$" * string(updOffOb.unitCost) * "}, " *
                            "is <= autoMatchMax of {\$" * string(demOb.autoMatchMax) * "}\n")
                        new_match_confirmed = true
                    else
                        # TBD in UIapps: prompt user forx decision
                        # auto-accepting here
                        new_match_confirmed = true
                    end
                    if new_match_confirmed == true
                        # for a new match iterate over suppl_IONpol
                        marketOffers[updOffID] = updOffOb
                        peer_IONpols_have_changed = true
                        # link offer updOff with this demand demNm
                        dem_offs_match[demNm] = Dict{String, String}()
                        dem_offs_match[demNm][updOffID] = "match option initialized"

                        for ionIt in keys(updOffOb.suppl_IONpol)
                            if !haskey(ION_terminate_cand, ionIt)
                                ION_terminate_cand[ionIt] = 0
                            end
                            ION_terminate_cand[ionIt] += demOb.unitsWanted * updOffOb.unitCost
                        end
                    end
                end
            end
        end
    end

    if peer_IONpols_have_changed
        # Pass 1: Gather the IONpol set that best describes what has changed
        # - latest IONpols for each nodeID I need to pay or want to buy from
        # - keyed by nodeID
        IONpols = Dict{String, IONpol_node_time}()
        for (demNm, offersMs) in dem_offs_match
            for (offID, offAtrr) in offersMs
                (offNm, offBy) = split(offID, "<<")
                if !haskey(IONpols, offBy)
                    IONpols[offBy] = IONpol_node_time(now(), offBy, marketOffers[offID].suppl_IONpol, 0)
                    IONpols[offBy].myMax = myDemands[demNm].unitsWanted * marketOffers[offID].unitCost
                else
                    if IONpols[offBy].timestamp < marketOffers[offID].timestamp
                        IONpols[offBy].timestamp = marketOffers[offID].timestamp
                        IONpols[offBy].peer_IONpol = marketOffers[offID].suppl_IONpol
                        # Regardless of how many IONs the supplier accepts,
                        # how many would I spend max on this dem-off match?
                        # - keep this in $ because divided by different IONs later
                        IONpols[offBy].myMax = myDemands[demNm].unitsWanted * marketOffers[offID].unitCost
                    end
                end
            end
        end
        for (payReceiv, accRecords) in accPayable
            for accRec in accRecords
                if !haskey(IONpols, payReceiv)
                    IONpols[payReceiv] = IONpol_node_time(now(), payReceiv, accRec.payReceiv_IONpol, 0)
                    IONpols[payReceiv].myMax = accRec.balance
                else
                    if IONpols[payReceiv].timestamp < accRec.timestamp
                        IONpols[payReceiv].timestamp = accRec.timestamp
                        IONpols[payReceiv].peer_IONpol = accRec.payReceiv_IONpol
                        # Regardless of how many IONs the payee accepts,
                        # how many would I spend max on this invoice?
                        # - keep this in $ because divided by different IONs later
                        IONpols[payReceiv].myMax = accRec.balance
                    end
                end
            end
        end

        # Pass 2: Go through the filtered set to create my own (new) IONpol
        my_IONpols_have_changed = false
        my_cand_IONPol = Dict{String, Dict{String, Float64}}()
        for (nodeID, IONpolWrap) in IONpols
            # for each ION in this nodeID's IONpol
            for (ion, pol) in IONpolWrap.peer_IONpol
                # skip IONs I mint myself, because I source their policy directly, not infer through others (loops possible otherwise)
                if !in(ion, IONs_I_mint)
                    my_IONpols_have_changed = true
                    ION_price = pol["ION_price"]
                    println(string(now()) * " @" * myID *
                        ": Looking at {" * ion * "} which is currently at unit price {\$" * string(ION_price) * "}")
                    peerMax_in_this_ION = pol["max_accept"]

                    if !haskey(ION_flow_candidates, ion)
                        ION_flow_candidates[ion] = Dict{String, Float64}()
                    end
                    if !haskey(ION_flow_candidates[ion], nodeID)
                        ION_flow_candidates[ion][nodeID] = 0
                    end
                    ION_flow_candidates[ion][nodeID] += peerMax_in_this_ION

                    println(string(now()) * " @" * myID *
                        ": ==> Peer {" * nodeID * "}'s capacity for {" * ion * "} is {" * string(peerMax_in_this_ION) * "} IONs\n")
                    myMax_in_this_ION = IONpolWrap.myMax / ION_price
                    println(string(now()) * " @" * myID *
                        ": ==> while my max capacity with this peer is {\$" * string(IONpolWrap.myMax) * "} which at current ION price means maximum {" * string(myMax_in_this_ION) * "} IONs\n")
                    min_of_maxs = min(peerMax_in_this_ION, myMax_in_this_ION)
                    if !haskey(my_cand_IONPol, ion)
                        my_cand_IONPol[ion] = Dict{String, Float64}()
                        println(string(now()) * " @" * myID *
                            ": ==> Will accept {" * ion * "} due to " * nodeID)
                    end
                    if haskey(my_cand_IONPol[ion], "max_accept")
                        my_cand_IONPol[ion]["max_accept"] += min_of_maxs
                        println(string(now()) * " @" * myID *
                            ": ==> Capacity for {" * ion * "} increased by {" * string(min_of_maxs) * "} IONs due to " * nodeID)
                        println(string(now()) * " @" * myID *
                            ": ==> Total capacity for {" * ion * "} is now {" * string(my_cand_IONPol[ion]["max_accept"]) * "} IONs \n")
                    else
                        my_cand_IONPol[ion]["max_accept"] = min_of_maxs
                        my_cand_IONPol[ion]["ION_price"] = ION_price
                        println(string(now()) * " @" * myID *
                            ": ==> Capacity for {" * ion * "} set to {" * string(min_of_maxs) * "} due to " * nodeID)
                        println(string(now()) * " @" * myID *
                            ": ==> Total capacity for {" * ion * "} is now {" * string(my_cand_IONPol[ion]["max_accept"]) * "} \n")
                    end
                else
                    println(string(now()) * " @" * myID *
                        ": New opportunity to spend {" * ion * "} IONs (which I mint) through node {" * nodeID * "} \n")
                end
            end
        end

        if my_IONpols_have_changed
            global myIONpol = deepcopy(my_cand_IONPol)

            # Pass 3: Use the new my_IONpol with all myOffers & accReceiv
            for (offNm, offOb) in myOffers
                offOb.timestamp = now()
                offOb.suppl_IONpol = my_cand_IONPol
            end
            for (paySender, accRecords) in accReceiv
                for accRec in accRecords
                    accRec.payReceiv_IONpol = my_cand_IONPol
                    accRec.timestamp = now()
                end
            end

            # Send updates & messages through Relay
            println(string(now()) * " @" * myID *
                ": My ION policies have changed, so I am updating the Relay for all my offers & I am sending direct messages to all those who need to pay me.\n")
            # broadcast updates for my offers through the Relay:
            HTTP.request("PUT", "http://127.0.0.1:8081/updateOffers/"*myID, [], JSON2.write(myOffers))
            # send direct messages to all paySenders to me, through the Relay:
            HTTP.request("POST", "http://127.0.0.1:8081/message_all_paySenders", [], JSON2.write(accReceiv))
        end
    end
end

#= function updateOffers(myID, myOffers)
    # to switch later based on web vs internal node
    # HTTP.request("PUT", "http://127.0.0.1:8081/updateOffers/"*myID, [], JSON2.write(myOffers))

    
end =#

task_pullRelay = @async while true; sleep(pullFreq); pullRelay_myMarketUpdate(); end

crash_alert_done = false
crashMonitor = @async while true
    sleep(1)
    if istaskfailed(task_pullRelay) && !crash_alert_done
        println("ERROR: task_pullRelay crashed")
        global crash_alert_done = true
    end
end
