using HTTP
using Sockets
using JSON2
using Dates

mutable struct Offer
    timestamp::DateTime
    offerName::String
    offerBy::String
    desc::String
    # Unit cost in $
    unitCost::Float64
    # ex: suppl_IONpol["CHOC"] = ("max_accept"=>"5")
    suppl_IONpol::Dict{String, Dict{String, Float64}}
end

# Market[{offer_name}<<{offered_by}] = Offer
const Market = Dict{String, Offer}()
# Market updates per each nodeID
# e.g. nodeMU["nodeX"][{offer_name}<<{offered_by}] = Offer
const nodeMU = Dict{String, Dict{String, Offer}}()

function processOffer(nodeID, offerName, offer)
    Market[offerName * "<<" * nodeID] = offer
    # Then, insert into any all nodeMUs except nodeID's
    for (k,v) in nodeMU
        if k != nodeID
            nodeMU[k][offerName * "<<" * nodeID] = offer
        end
    end
end

# Note: newOffer and updateOffers differ in POST vs PUT
function newOffer(req::HTTP.Request)
    nodeID = HTTP.URIs.splitpath(req.target)[2]
    offerName = HTTP.URIs.splitpath(req.target)[3]
    offer = JSON2.read(IOBuffer(HTTP.payload(req)), Offer)
    processOffer(nodeID, offerName, offer)
    return HTTP.Response(200)
end

function HTTPupdateOffers(req::HTTP.Request)
    nodeID = HTTP.URIs.splitpath(req.target)[2]
    offers = JSON2.read(IOBuffer(HTTP.payload(req)), Dict{String, Offer})
    
    updateOffers(nodeID, offers)

    return HTTP.Response(200)
end

function updateOffers(nodeID, offers)
    for (offNm, offOb) in offers
        processOffer(nodeID, offNm, offOb)
    end
end

function getMyMarketUpdate(req::HTTP.Request)
    nodeID = HTTP.URIs.splitpath(req.target)[2]

    # If first time, send full Market
    # and create nodeMU for nodeID
    if !haskey(nodeMU, nodeID)
        nodeMU[nodeID] = Dict{String, Offer}()
        nodeAU[nodeID] = Vector{AccRecord}()
        nodeFSM[nodeID] = Vector{Flow_Setup_Message}()
        toSend = Market
    else
        # Otherwise send only nodeMU
        toSend = deepcopy(nodeMU[nodeID])
        empty!(nodeMU[nodeID])
    end
    return HTTP.Response(200, JSON2.write(toSend))
end

function getFullMarket(req::HTTP.Request)
    return HTTP.Response(200, JSON2.write(Market))
end

function deleteOffer(req::HTTP.Request)
    nodeID = HTTP.URIs.splitpath(req.target)[2]
    offerName = HTTP.URIs.splitpath(req.target)[3]

    # delete from Market and from any nodeMUs
    #delete!(Market, nodeID)

    return HTTP.Response(200)
end

mutable struct AccRecord
    timestamp::DateTime
    invID::String
    paySender::String
    payReceiv::String
    balance::Float64
    # ex: payReceiv_IONpol["CHOC"] = ("max_accept"=>5)
    payReceiv_IONpol::Dict{String, Dict{String, Float64}}
end

# Accounting updates for each nodeID
# e.g. nodeAU["nodeX"] = Vector{AccRecord}
const nodeAU = Dict{String, Vector{AccRecord}}()

function message_all_paySenders(req::HTTP.Request)
    accReceiv = JSON2.read(IOBuffer(HTTP.payload(req)), Dict{String, Vector{AccRecord}})
    for (paySender, accRecords) in accReceiv
        for accRec in accRecords
            if !haskey(nodeAU, paySender)
                nodeAU[paySender] = Vector{AccRecord}()
            end
            push!(nodeAU[paySender], accRec)
        end
    end
    return HTTP.Response(200)
end

function message_paySender(req::HTTP.Request)
    paySender = HTTP.URIs.splitpath(req.target)[2]
    accRec = JSON2.read(IOBuffer(HTTP.payload(req)), AccRecord)
    if !haskey(nodeAU, paySender)
        nodeAU[paySender] = Vector{AccRecord}()
    end
    push!(nodeAU[paySender], accRec)
    return HTTP.Response(200)
end

function getMyAccountingMessages(req::HTTP.Request)
    nodeID = HTTP.URIs.splitpath(req.target)[2]
    if haskey(nodeAU, nodeID)
        toSend = deepcopy(nodeAU[nodeID])
        empty!(nodeAU[nodeID])
        return HTTP.Response(200, JSON2.write(toSend))
    else
        return HTTP.Response(200)
    end
end

mutable struct ION_contract
    contract_uuid::String
    ion_name::String
    ion_price::Float64
    offerID::String
    creation_timestamp::DateTime
    # Current implementation allows for a single minter
    allowed_minters::String
    # Nodes that can spend this ion & who participate in its management. Dictionary holds how much each node has of the contract.
    stakeholders::Dict{String, Float64}
    # The total units across all stakeholders
    total::Float64
    # Additional fields would vary depending on how the stakeholders of any contract chose to manage it. Here in the simulator we use the simplest form of minter discretion with stakeholder revocation if requested.
end

# ex: ION_contracts["CHOC"][uuid] = ION_contract
const ION_contracts = Dict{String, Dict{String, ION_contract}}()

function newIONcontract(req::HTTP.Request)
    new_contract = JSON2.read(IOBuffer(HTTP.payload(req)), ION_contract)
    # No additional steps needed because anyone can propose any new contract. No financial implications until stakeholders begin joining, if ever.
    if !haskey(ION_contracts, new_contract.ion_name)
        ION_contracts[new_contract.ion_name] = Dict{String, ION_contract}()
    end
    ION_contracts[new_contract.ion_name][new_contract.contract_uuid] = new_contract
    return HTTP.Response(200)
end

function updateIONcontract(req::HTTP.Request)
    updateType = HTTP.URIs.splitpath(req.target)[2]
    updContract = JSON2.read(IOBuffer(HTTP.payload(req)), ION_contract)
    # For this simulator we can just replace, but in general an update will have to comply with contract policy and might involve stakeholder communication.
    if updateType == "minter_adding_ION_units"
        ION_contracts[updContract.ion_name][updContract.contract_uuid] = updContract
    end
    return HTTP.Response(200)
end

# mutable struct Supply_or_Demand_Components
#     # Dictionaries allow for attributes insertion later
#     # ex: s_d_names[{x_name_WITHOUT<<nodeID}] = string_of_attributes
#     s_d_names::Dict{String, String}
#     # keyes by invoice uuid
#     invoiceIDs::Dict{String, String}
# end

mutable struct Flow_Setup_Message
    flow_uuid::String
    ion_name::String
    ion_price::Float64
    flow_state::String
    # In $s
    flow_size::Float64
    ion_contract_uuid::String
    sent_from::String
    # Relay processes and forwards to this node
    next_node::String
    # Flows run from demand components to supply components (can be thought of as the direction of payments-made)
    # The demand components of this flow leg (demands, account_payables)
    inflow_comp::Dict{String, String}
    # The supply components of this flow leg (offers, account_receivables)
    outflow_comp::Dict{String, String}
end

# ex. flows[flow_uuid] = Dict("flow_start_porposal"=>FSM, "next_node"=>FSM, etc.)
const flows = Dict{String, Dict{String, Flow_Setup_Message}}()

# Flow setup message queues for each nodeID
const nodeFSM = Dict{String, Vector{Flow_Setup_Message}}()

function sendFlowSetupMsg(req::HTTP.Request)
    fsMsg = JSON2.read(IOBuffer(HTTP.payload(req)), Flow_Setup_Message)
    if ~haskey(flows, fsMsg.flow_uuid)
        flows[fsMsg.flow_uuid] = Dict{String, Flow_Setup_Message}()
    end

    if fsMsg.flow_state == "flow_start_proposal"

        # This is where D's ION spend & delete is filtered

        flows[fsMsg.flow_uuid]["flow_start_proposal"] = fsMsg
    else
        flows[fsMsg.flow_uuid][fsMsg.sent_from] = fsMsg
    end
    if !haskey(nodeFSM, fsMsg.next_node)
        nodeFSM[fsMsg.next_node] = Vector{Flow_Setup_Message}()
    end
    push!(nodeFSM[fsMsg.next_node], fsMsg)
    #println("nodeFSM: ", nodeFSM)
    return HTTP.Response(200)
end

function getFlowSetupMsg(req::HTTP.Request)
    nodeID = HTTP.URIs.splitpath(req.target)[2]
    if haskey(nodeFSM, nodeID)
        toSend = deepcopy(nodeFSM[nodeID])
        empty!(nodeFSM[nodeID])
        return HTTP.Response(200, JSON2.write(toSend))
    else
        return HTTP.Response(200)
    end
end

function validate_flow(fsMsg::Flow_Setup_Message)
    if fsMsg.flow_state == "flow_end_proposal"
        return true
    end
    if haskey(flows[fsMsg.flow_uuid], fsMsg.next_node)
        return validate_flow(flows[fsMsg.flow_uuid][fsMsg.next_node])
    else
        return false
    end
    # return true # for multple next_nodes version
end

function validate_flow_candidates()
    for flowDict in values(flows)
        is_valid = validate_flow(flowDict["flow_start_proposal"])
        if is_valid
            println("Valid flow: ", flowDict["flow_start_proposal"].flow_uuid)
            fsMsg = deepcopy(flowDict["flow_start_proposal"])
            fsMsg.flow_state = "flow_confirmation"
            fsMsg.sent_from = "Relay"
            fsMsg.inflow_comp = Dict{String, String}()
            fsMsg.outflow_comp = Dict{String, String}()
            for nodeID in keys(nodeFSM)
                push!(nodeFSM[nodeID], fsMsg)
            end
        end
    end
end

#= task_validate = @async while true; sleep(2); validate_flow_candidates(); end

crash_alert_done = false
crashMonitor = @async while true
    sleep(1)
    if istaskfailed(task_validate) && !crash_alert_done
        println("ERROR: task_validate crashed")
        global crash_alert_done = true
    end
end =#

function audSim_wants_full_relay_dump(req::HTTP.Request)

    return HTTP.Response(200, "Ok")
end

# define REST endpoints to dispatch to "service" functions
const RELAY_ROUTER = HTTP.Router()
HTTP.@register(RELAY_ROUTER, "PUT", "/sendFlowSetupMsg", sendFlowSetupMsg)
HTTP.@register(RELAY_ROUTER, "GET", "/getFlowSetupMsg", getFlowSetupMsg)

HTTP.@register(RELAY_ROUTER, "PUT", "/newIONcontract", newIONcontract)
HTTP.@register(RELAY_ROUTER, "PUT", "/updateIONcontract/*", updateIONcontract)

HTTP.@register(RELAY_ROUTER, "PUT", "/newOffer", newOffer)
HTTP.@register(RELAY_ROUTER, "PUT", "/updateOffers", HTTPupdateOffers)
# note the use of `*` to capture the nodeId
HTTP.@register(RELAY_ROUTER, "GET", "/getMyMarketUpdate/*", getMyMarketUpdate)
HTTP.@register(RELAY_ROUTER, "DELETE", "/deleteOffer/*", deleteOffer)

HTTP.@register(RELAY_ROUTER, "GET", "/getFullMarket", getFullMarket)

HTTP.@register(RELAY_ROUTER, "POST", "/message_all_paySenders", message_all_paySenders)
HTTP.@register(RELAY_ROUTER, "POST", "/message_paySender/*", message_paySender)
HTTP.@register(RELAY_ROUTER, "GET", "/getMyAccountingMessages", getMyAccountingMessages)

HTTP.@register(RELAY_ROUTER, "GET", "/audSim_wants_full_relay_dump", audSim_wants_full_relay_dump)

@async HTTP.serve(RELAY_ROUTER, Sockets.localhost, 8081, verbose=true)
