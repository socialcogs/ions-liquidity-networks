// Adds zoom buttons in top, left corner
var buttons = document.createElement('div');
buttons.style.position = 'absolute';
buttons.style.overflow = 'visible';

var bs = graph.getBorderSizes();
buttons.style.top = (container.offsetTop + bs.y) + 'px';
buttons.style.left = (container.offsetLeft + bs.x) + 'px';

var left = 0;
var bw = 16;
var bh = 16;

function addButton(label, funct)
{
    var btn = document.createElement('div');
    mxUtils.write(btn, label);
    btn.style.position = 'absolute';
    btn.style.backgroundColor = 'transparent';
    btn.style.border = '1px solid gray';
    btn.style.textAlign = 'center';
    btn.style.fontSize = '10px';
    btn.style.cursor = 'hand';
    btn.style.width = bw + 'px';
    btn.style.height = bh + 'px';
    btn.style.left = left + 'px';
    btn.style.top = '0px';
    
    mxEvent.addListener(btn, 'click', function(evt)
    {
        funct();
        mxEvent.consume(evt);
    });
    
    left += bw;
    
    buttons.appendChild(btn);
};

addButton('+', function()
{
    graph.zoomIn();
});

addButton('-', function()
{
    graph.zoomOut();
});

if (container.nextSibling != null)
{
    container.parentNode.insertBefore(buttons, container.nextSibling);
}
else
{
    container.appendChild(buttons);
}