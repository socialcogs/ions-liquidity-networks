// Displaying an XML string into an existing graph
var graph = new mxGraph(container);
   // or any existing graph
var xmlDocument = mxUtils.parseXml(xml);
var decoder = new mxCodec(xmlDocument);
decoder.decode(xmlDocument.documentElement, graph.getModel());

// Prevent graph interactions (moving, editing)
graph.setEnabled(false);

// Changes the default style for edges "in-place"
var style = graph.getStylesheet().getDefaultEdgeStyle();
style[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;

// Enables panning with left mouse button
graph.panningHandler.useLeftButtonForPanning = true;
graph.panningHandler.ignoreCell = true;
graph.container.style.cursor = 'move';
graph.setPanning(true);

// if the container should be resized to the graph size when the graph size has changed.  Default is false.
graph.resizeContainer = true;