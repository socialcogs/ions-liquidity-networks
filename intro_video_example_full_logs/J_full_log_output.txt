2020-08-11T18:07:54.949 @J: Got offers updates from Relay

Offers in update: Dict{String,Offer}("skill_x<<D" => Offer(2020-08-11T18:07:50.596, "skill_x", "D", "some description...", 10.0, Dict{String,Dict{String,Float64}}()))


2020-08-11T18:08:07.792 @J: Got accounting messages from Relay

Accounting messages in update: AccRecord[AccRecord(2020-08-11T18:08:07.104, "229d1e08-dc1f-11ea-1f13-43a2532b2fa8", "J", "L", 20.0, Dict{String,Dict{String,Float64}}())]


julia> Add_Offer("cookingSupplies", 36)

julia> 2020-08-11T18:08:35.01 @J: Got offers updates from Relay

Offers in update: Dict{String,Offer}("chocolate<<C" => Offer(2020-08-11T18:08:34.519, "chocolate", "C", "some description...", 10.0, Dict("CHOC" => Dict("max_accept" => 100.0,"ION_price" => 10.0))))


2020-08-11T18:08:38.138 @J: Got offers updates from Relay

Offers in update: Dict{String,Offer}("skill_x<<D" => Offer(2020-08-11T18:08:37.574, "skill_x", "D", "some description...", 10.0, Dict("CHOC" => Dict("max_accept" => 5.0,"ION_price" => 10.0))))


2020-08-11T18:08:41.146 @J: Got accounting messages from Relay

Accounting messages in update: AccRecord[AccRecord(2020-08-11T18:08:38.187, "229d1e08-dc1f-11ea-1f13-43a2532b2fa8", "J", "L", 20.0, Dict("CHOC" => Dict("max_accept" => 3.0,"ION_price" => 10.0)))]


2020-08-11T18:08:41.149 @J: ION policy for {L} has changed - will process it.

2020-08-11T18:08:41.15 @J: Looking at {CHOC} which is currently at unit price {$10.0}
2020-08-11T18:08:41.15 @J: ==> Peer {L}'s capacity for {CHOC} is {3.0} IONs

2020-08-11T18:08:41.15 @J: ==> while my max capacity with this peer is {$20.0} which at current ION price means maximum {2.0} IONs

2020-08-11T18:08:41.15 @J: ==> Will accept {CHOC} due to L
2020-08-11T18:08:41.15 @J: ==> Capacity for {CHOC} set to {2.0} due to L
2020-08-11T18:08:41.15 @J: ==> Total capacity for {CHOC} is now {2.0} 

2020-08-11T18:08:41.15 @J: My ION policies have changed, so I am updating the Relay for all my offers & I am sending direct messages to all those who need to pay me.

2020-08-11T18:08:59.604 @J: Got Flow Setup messages from Relay

Flow Setup messages in update: Flow_Setup_Message[Flow_Setup_Message("41a5aa2a-dc1f-11ea-1fea-713204a818a2", "CHOC", 10.0, "flow_start_proposal", 20.0, "3d9e733c-dc1f-11ea-1f13-43a2532b2fa8", "C", "J", Dict("cookingSupplies" => "market_entry"), Dict("cookingSupplies" => "market_entry"))]


2020-08-11T18:08:59.814 @J: I am a pass-through for {CHOC} IONs

2020-08-11T18:08:59.835 @J: ==> Sent {flow_LEG_proposal} to Relay with next node {L} and its outflow_comp component {Dict("229d1e08-dc1f-11ea-1f13-43a2532b2fa8" => "accounting_entry")}

2020-08-11T18:09:45.078 @J: Got Flow Setup messages from Relay

Flow Setup messages in update: Flow_Setup_Message[Flow_Setup_Message("41a5aa2a-dc1f-11ea-1fea-713204a818a2", "CHOC", 10.0, "flow_confirmation", 20.0, "3d9e733c-dc1f-11ea-1f13-43a2532b2fa8", "Relay", "J", Dict{String,String}(), Dict{String,String}())]


2020-08-11T18:09:45.081 @J: Flow {41a5aa2a-dc1f-11ea-1fea-713204a818a2} confirmed by the Relay

