# Economic Liquidity Internet IONs

This repository has all the code for ION liquidity networks as described at https://socialcogs.net/. Please see the site for the whitepaper and theoretical work behind the platform.

The simulator allows one to define any number of economic participants with their supply, demand, and accounting relationships, and then run ION network algorithms.

## Prerequisites

1. [Download Julia](https://julialang.org/downloads/) and install Julia according to istructions for your platform.
2. Clone this repository, change to the target directory, execute:
   ```
   julia --project=. init.jl
   ```

## Step-by-step Scenario

Below are step-by-step setup of the scenario from the introduction video https://youtu.be/pzpI2-2Ad-0. If you don't want to run it yourself, you can follow all the network events through the saved log outputs from each node available in folder intro_video_example_full_logs.

1. Start the Relay node:
   ```
   $ julia --project=. -i src/web_relay.jl
   ```

2. Start each of the four nodes in separate terminal windows:
   ```
   # Console C
   $ julia --project=. -i src/web_client.jl C 3

   # Console D
   $ julia --project=. -i src/web_client.jl D 3

   # Console J
   $ julia --project=. -i src/web_client.jl J 3

   # Console L
   $ julia --project=. -i src/web_client.jl L 3
   ```

3. At the console for D:
   ```
   Add_Demand("chocolate", 5, 50)
   Add_Offer("skill_x", 10)
   ```
   We see that everyone sees D's offer

4. At the console for L:
   ```
   Add_Demand("skill_x", 3, 50)
   ```
   Right after posting its demand, L's app searches the Market and a match confirmed with D. However, D is not accepting any IONs yet, and L does not have money to complete the transaction.

5. At the console for L:
   ```
   Send_Invoice("J", 20)
   ```
   L sends an invoice to J. We see that J gets the invoice, but cannot act on it because J does not have money.

6. At the console for J:
   ```
   Add_Offer("cookingSupplies", 36)
   ```

7. At the console for C:
   ```
   Add_Demand("cookingSupplies", 10, 50)
   ```
   We see that C matches with J's offer, but cannot act on it because C does not have money.

8. At the console for C:
   ```
   Add_Offer("chocolate", 10, "CHOC", 10, 100)
   ```
   With this command, C offers its product using the "CHOC" IONs.
   We see D noticing and matching with C's offer, and starting to accept 5 CHOC IONs. We also see D's app alerting the market and accounting peers for the new ION flow options. This triggers a burst of activity as the network detects the flow opportunity:
   - L's app sees that she could now pay D for skill_x using CHOC, and since L wants only $30 worth of D's skills, L's app advertises acceptance of 3 IONs of CHOC.
   - Then J sees that he can pay L's invoice with CHOC, and since J only needs to pay $20, J's app advertises its acceptance of 2 IONs of CHOC
   
   C's app now sees that she is able to pay J using her own CHOC IONs.
   C doesn't have any "CHOC" IONs minted yet, which she does with:

9. At the console for C:
   ```
   Mint_IONs("chocolate", "CHOC", 7)
   ```
   We see the new ION contract inserted into the wallet and synced with the Relay. Now C can place an order with J:

10. At the console for C:
    ```
    Order_and_Pay_using_IONs("cookingSupplies<<J", "CHOC", 2)
    ```
    We see ION flow proposals starting with C and cascading automatically with each node, searching for a complete flow solution.
    The the network detects a complete flow, it alerts all the nodes, and triggers the related accounting, in effect financially settling all their transactions. 
